package com.ally.poc.s3;


import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.SSEAwsKeyManagementParams;
import com.amazonaws.services.s3.model.PutObjectResult;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;

public class UploadObject {

    public static void main(String[] args) throws IOException {
        Regions clientRegion = Regions.US_EAST_1;
        String bucketName = "qzm1gx-cloud-lab-101-104964-default-us-east-1-qzm1gx";
        String stringObjKeyName = "forms.json"; //+System.currentTimeMillis();
//        String fileName ="message.txt";
//        String fileObjKeyName = "message.txt";

	    String objectContent = "Test object encrypted with SSE";
        byte[] objectBytes = objectContent.getBytes();       
        
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(clientRegion)
                .build();
        
        SSEAwsKeyManagementParams sSEAwsKeyManagementParams = new SSEAwsKeyManagementParams("arn:aws:kms:us-east-1:650487181597:key/02da882d-59cb-471f-9ff3-dc89a13fd255");
        
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(objectBytes.length);
        metadata.setContentType("application/json");
        metadata.addUserMetadata("title", "forms.json");
        
        URL url = uploadObject(s3Client, sSEAwsKeyManagementParams,metadata, bucketName, stringObjKeyName, objectBytes);
        
        System.out.println("Pre-Signed URL: " + url.toString());
    }

	private static URL uploadObject( AmazonS3 s3Client,  SSEAwsKeyManagementParams sSEAwsKeyManagementParams, ObjectMetadata metadata, String bucketName, String stringObjKeyName,byte[] objectBytes) {
		  URL url = null;
		try {
            //This code expects that you have AWS credentials set up per:
            // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html
           
/*  PutObjectRequest putRequest = new PutObjectRequest(bucketName, stringObjKeyName, new File(fileObjKeyName))
					.withSSEAwsKeyManagementParams(new SSEAwsKeyManagementParams("arn:aws:kms:us-east-1:650487181597:key/02da882d-59cb-471f-9ff3-dc89a13fd255"));
            
            putRequest.putCustomRequestHeader("x-amz-server-side-encryption","aws:kms");
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("plain/text");
            metadata.addUserMetadata("title", "someTitle");
            putRequest.setMetadata(metadata);
            s3Client.putObject(putRequest);
  */

            PutObjectRequest putRequest = new PutObjectRequest(bucketName,
            		stringObjKeyName,
                    new ByteArrayInputStream(objectBytes),
                    metadata)
					.withSSEAwsKeyManagementParams(sSEAwsKeyManagementParams);
            putRequest.putCustomRequestHeader("x-amz-server-side-encryption","aws:kms");
            
            PutObjectResult result= s3Client.putObject(putRequest);
    	    
            System.out.println("Object uploaded..with SSEAwsKmsKeyId..."+result.getMetadata().getSSEAwsKmsKeyId()+" ..!");
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = Instant.now().toEpochMilli();
            expTimeMillis += 1000 * 60 * 60 * 24 * 8 ; // valid for 8 days
            expiration.setTime(expTimeMillis);

            // Generate the presigned URL.
            System.out.println("Generating pre-signed URL.");
          
            GeneratePresignedUrlRequest generatePresignedUrlRequest =
                    new GeneratePresignedUrlRequest(bucketName, stringObjKeyName)
                            .withMethod(HttpMethod.GET)
                            .withExpiration(expiration);
            url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

            System.out.println("Pre-Signed URL: " + url.toString()+" valid till -"+generatePresignedUrlRequest.getExpiration());
         
       
		} catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
            e.printStackTrace();
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
	
		   return url;
	}
}


