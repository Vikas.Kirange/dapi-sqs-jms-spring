package com.ally.poc.s3;

import java.util.Arrays;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.SimpleRegistry;
import org.apache.camel.spi.Debugger;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

public class S3PutObjectWithCamelComponent extends CamelTestSupport {
	private static AWSCredentialsProvider credentialsProvider = new ProfileCredentialsProvider(
			"C:\\Users\\nzzptg\\.aws\\credentials", "saml");;

	public static void main(String args[]) throws Exception {
		AmazonSQS client = AmazonSQSClientBuilder.standard().withCredentials(credentialsProvider)
				.withRegion(Regions.US_EAST_1).build();

		System.out.println("client properties.." + client.ENDPOINT_PREFIX);
		System.out.println("AmazonSQS client.listQueues  is --" );

		SimpleRegistry registry = new SimpleRegistry();
		registry.putIfAbsent("client", client);
		registry.putIfAbsent("useExchangeId", "useExchangeId");

		try {
			CamelContext context = new DefaultCamelContext(registry);
			context.setTracing(true);
		
			System.out.println("Camel context is .." + context.getComponentNames().toString());
			System.out.println("Endpoint size is .." + context.getEndpoints().size());
			System.out.println("Endpoint context is .." + context.getEndpoints().toString());
			System.out.println("Endpoint is .." + Arrays.toString(context.getEndpoints().toArray()));

			S3PutObjectWithCamelComponent tx = new S3PutObjectWithCamelComponent();
			context.addRoutes(tx.createRouteBuilder());
			//			ProducerTemplate template = context.createProducerTemplate();
			context.start();
			System.out.println("Context started in main....");
			Thread.sleep(10000);
			context.stop();
			System.out.println("Camel published message on aws queue using jms component....");
		} catch (Exception e) {
			System.out.println("Exception is ..." + e.getMessage());
			e.printStackTrace();
		}
	}

//		try {
//			TextMessageSenderWithCamelComponent tx = new TextMessageSenderWithCamelComponent();
//	
//			context.setTracing(true);
//			context.addRoutes(new StandardQueueRoute());
//			context.start();
//			System.out.println("Context started in main ...using spring configuration. standard queue..");
//			Thread.sleep(10000);
//			context.stop();

//			context.addRoutes(new FiFoQueueRoute());
//			context.start();
//			System.out.println("Context started in main ...using spring configuration. fifo queue..");
//			Thread.sleep(10000);
//			context.stop();

//			System.out.println("Camel published message on aws queue using jms component....");
//		} catch (Exception e) {
//			System.out.println("Exception is ..." + e.getMessage());
//			e.printStackTrace();
//		}
//	}

	@Override
	protected RouteBuilder createRouteBuilder() throws Exception {
		return new RouteBuilder() {
			@Override
			public void configure() {
				System.out.println("Configure the error handler....");
				errorHandler(deadLetterChannel("direct:dead").useOriginalMessage());

				
//				from("direct:start").transform(constant("This a message from service.."))
//						// .to("aws-sqs:nzzptg-dapi-sqs-poc-dev-f-tf-starter-dapi-sqs-poc-queue-one.fifo?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false&messageGroupIdStrategy=useExchangeId")
//						.to("aws-sqs:arn:aws:sqs:us-east-1:650487181597:nzzptg-dapi-sqs-poc-default-dapi-sqs-poc-queue-one.fifo?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false&messageGroupIdStrategy=useExchangeId")
//						.throwException(new IllegalArgumentException("Forced"));
//

				from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true")
				.to("aws-sqs:arn:aws:sqs:us-east-1:650487181597:nzzptg-dapi-sqs-poc-default-dapi-sqs-poc-queue-one.fifo?amazonSQSClient=#client&messageGroupIdStrategy=#useExchangeId");
//				.throwException(new IllegalArgumentException("Forced"));
				
				// the dead letter route where we call a bean to prepare the message and send it
				// to mock endpoint.

				//from("direct:dead").bean(new FailureBean())
//				from("file:data/inbox?noop=true").bean(new FailureBean())
//						.to("aws-sqs:arn:aws:sqs:us-east-1:650487181597:nzzptg-dapi-sqs-poc-104964-default-dapi-sqs-poc-queue-one?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false")
//						//.to("mock:dead");
//				.to("mock:end");

//				from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true")
//						.to("aws-sqs:nzzptg-dapi-sqs-poc-dev-f-tf-starter-dapi-sqs-poc-queue-one.fifo?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false").throwException(new Exception("..ppppublisheed"))
//						.to("mock:end");
				System.out.println("After route configure....");
			}

		};
//		return new RouteBuilder() {
//			@Override
//			public void configure() {
//				System.out.println("Inside configure....");
//				onException(Exception.class).process(new FailureProcessor()).log("Received body ... ").handled(true).maximumRedeliveries(1);
//
//				// from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true").to("jms:dapi-aws-sqs-poc").to("mock:end");
//				from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true")
//						.to("aws-sqs:nzzptg-dapi-sqs-poc-dev-f-tf-starter-dapi-sqs-poc-queue-one.fifo?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false").throwException(new Exception("..ppppublisheed"))
//						.to("mock:end");
//				System.out.println("After route configure....");
//			}
//
//		};
	}
}