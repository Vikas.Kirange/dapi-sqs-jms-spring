package com.ally.poc.s3;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cloudtrail.model.KmsException;
import software.amazon.awssdk.services.kms.model.EncryptRequest;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.Owner;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.SSEAwsKeyManagementParams;
import com.amazonaws.services.s3.model.SSECustomerKey;

import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kms.KmsClient;
import software.amazon.awssdk.services.kms.model.EncryptResponse;

/**
 * Upload a file to an Amazon S3 bucket.
 * 
 * This code expects that you have AWS credentials set up per:
 * http://docs.aws.amazon.com/java-sdk/latest/developer-guide/setup-credentials.html
 */
public class PutObject {

	private static AWSCredentialsProvider credentialsProvider = new ProfileCredentialsProvider(
			"C:\\Users\\nzzptg\\.aws\\credentials", "saml");;

	public static void main(String[] args) {

		String bucket_name = "arn:aws:s3:::qzm1gx-cloud-lab-101-104964-default-us-east-1-qzm1gx";
		String file_path = "H:\\Repo\\dapi-sqs-jms-spring\\data\\inbox\\message.txt";
		String s3_object_key = "Message1"; // "s3:x-amz-server-side-encryption";
		String s3_encryption_key = "arn:aws:kms:us-east-1:650487181597:alias/aws/s3"; 
		System.out.format("Uploading %s to S3 bucket %s ...\n", file_path, bucket_name);
		final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withCredentials(credentialsProvider)
				.withRegion(Regions.US_EAST_1).build();

		try {
//			Owner owner = s3.getS3AccountOwner();
//
//			System.out.println("Owner of buckets is: " + owner.getDisplayName());
//
//			List<Bucket> buckets = s3.listBuckets();
//			System.out.println("Your {S3} buckets are:");
//			for (Bucket b : buckets) {
//				System.out.println("* " + b.getName());
//			}
			
			PutObjectRequest putRequest = new PutObjectRequest(bucket_name,s3_object_key, new File(file_path));
			 
			//PutObjectResult putObjectResult = s3.putObject(putRequest);
			
			
			SSEAwsKeyManagementParams sseEncryptionKey= putRequest.getSSEAwsKeyManagementParams();
			
			if (null!= sseEncryptionKey) {
			System.out.println("SSEAwsKeyManagementParams sseEncryptionKey is :"+sseEncryptionKey.getAwsKmsKeyId());
			}
			else {
				System.out.println("SSEAwsKeyManagementParams sseEncryptionKey is null...");
			}
			
			SSECustomerKey encryptionKey =	putRequest.getSSECustomerKey();
			if(null != encryptionKey) {
			System.out.println("SSECustomerKey sseEncryptionKey is in base64 format is :"+encryptionKey.getKey());
			}
			else {
				System.out.println("SSECustomerKey sseEncryptionKey is in base64 format is null...");
			}
			
//			PutObjectRequest putRequest = new PutObjectRequest(bucket_name, s3_object_key, new File(file_path))
//					.withSSEAwsKeyManagementParams(new SSEAwsKeyManagementParams());
			SSEAwsKeyManagementParams sseEncryptionKey1 = new SSEAwsKeyManagementParams(s3_encryption_key);
			putRequest = new PutObjectRequest(bucket_name, s3_object_key, new File(file_path))
					.withSSEAwsKeyManagementParams(sseEncryptionKey1);
			s3.putObject(putRequest);
		byte[] encryptedData =	encryptData(sseEncryptionKey.getAwsKmsKeyId(), getObjectFile(file_path));

			//s3.putObject(putRequest, RequestBody.fromBytes(encryptedData));

			System.out.println("After put request!..");
			// s3.putObject(bucket_name, key_name, new File(file_path));

		} catch (AmazonServiceException e) {
			System.err.println(e.getErrorMessage());
			System.exit(1);
		}
		System.out.println("Done!");
	}

	private static byte[] encryptData(String keyId, byte[] data) {
		try {
			KmsClient kmsClient = getKMSClient();
			SdkBytes myBytes = SdkBytes.fromByteArray(data);

			EncryptRequest encryptRequest = EncryptRequest.builder().keyId(keyId).plaintext(myBytes).build();

			EncryptResponse response = kmsClient.encrypt(encryptRequest);
			String algorithm = response.encryptionAlgorithmAsString().toString();
			System.out.println("The encryption algorithm is " + algorithm);

			SdkBytes encryptedData = response.ciphertextBlob();

			return encryptedData.asByteArray();

		} catch (KmsException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return null;
	}

	
	
	private static KmsClient getKMSClient() {
		Region region = Region.US_EAST_1;

		KmsClient kmsClient = KmsClient.builder().region(region).build();
		
		return kmsClient;
	}

	private static byte[] getObjectFile(String filePath) {

		FileInputStream fileInputStream = null;
		byte[] bytesArray = null;
		try {
			File file = new File(filePath);
			bytesArray = new byte[(int) file.length()];

			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bytesArray);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return bytesArray;
	}
}
