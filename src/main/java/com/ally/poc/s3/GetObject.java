package com.ally.poc.s3;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
* Get an object within an Amazon S3 bucket.
* 
* This code expects that you have AWS credentials set up per:
* http://docs.aws.amazon.com/java-sdk/latest/developer-guide/setup-credentials.html
*/
public class GetObject {
 public static void main(String[] args) {
     String bucketName = "qzm1gx-cloud-lab-101-104964-default-us-east-1-qzm1gx";
     String key_name = "myString11651842098163";
     
     Regions clientRegion = Regions.US_EAST_1;
  //   String stringObjKeyName = "myString1"+System.currentTimeMillis(); //myString11651842098163

     System.out.format("Downloading %s from S3 bucket %s...\n", key_name, bucketName);
     final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(clientRegion).build();
     try {
         S3Object o = s3.getObject(bucketName, key_name);
         S3ObjectInputStream s3is = o.getObjectContent();
         FileOutputStream fos = new FileOutputStream(new File(key_name));
         byte[] read_buf = new byte[1024];
         int read_len = 0;
         while ((read_len = s3is.read(read_buf)) > 0) {
             fos.write(read_buf, 0, read_len);
         }
         s3is.close();
         fos.close();
     } catch (AmazonServiceException e) {
         System.err.println(e.getErrorMessage());
         System.exit(1);
     } catch (FileNotFoundException e) {
         System.err.println(e.getMessage());
         System.exit(1);
     } catch (IOException e) {
         System.err.println(e.getMessage());
         System.exit(1);
     }
     System.out.println("Done!");
 }
}

