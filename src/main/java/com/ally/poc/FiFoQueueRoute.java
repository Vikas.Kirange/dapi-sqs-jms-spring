package com.ally.poc;

import org.apache.camel.builder.RouteBuilder;

public class FiFoQueueRoute extends RouteBuilder {

	private String  routeId ="fifoRoute";
	
	@Override
	public void configure() throws Exception {
		System.out.println("Inside configure....");
		onException(Exception.class).process(new FailureProcessor()).log("Received body ... ").handled(true)
				.maximumRedeliveries(1);
		from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true")
				.to("aws-sqs:nzzptg-dapi-sqs-poc-dev-f-tf-starter-dapi-sqs-poc-queue-one.fifo?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false&messageGroupIdStrategy=useExchangeId")
				.throwException(new Exception("..ppppublisheed")).to("mock:end");

		System.out.println("After route configure....");
	}

}
