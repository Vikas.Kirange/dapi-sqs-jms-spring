package com.ally.poc;

import org.apache.camel.builder.RouteBuilder;
import com.ally.poc.FailureProcessor;

public class StandardQueueRoute extends RouteBuilder {

	private String  routeId ="standardRoute";
	
	@Override
	public void configure() throws Exception {
		System.out.println("Inside StandardQueueRoute configure....");
		onException(Exception.class).process(new FailureProcessor()).log("Received body ... ").handled(true)
				.maximumRedeliveries(1);
		from("file:data/inbox?noop=true").to("log:?level=DEBUG&showBody=true&showHeaders=true")
				.to("aws-sqs:arn:aws:sqs:us-east-1:650487181597:nzzptg-dapi-sqs-poc-104964-dev-dapi-sqs-poc-queue-one?amazonSQSClient=#client&defaultVisibilityTimeout=5000&deleteAfterRead=false")
				.throwException(new Exception("..ppppublisheed")).to("mock:end");
			System.out.println("After StandardQueueRoute route configure....");
	}

}

