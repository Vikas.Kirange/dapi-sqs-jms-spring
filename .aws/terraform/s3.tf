# https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-s3
module "bucket" {
  source        = "git::ssh://git@bitbucket.int.ally.com/tf/s3.git?ref=v2"
  name          = "nzzptg-q-poc"
  force_destroy = var.environment != "prod"
  namespace     = module.namespace.lower_short_name
  tags          = module.namespace.tags
}
