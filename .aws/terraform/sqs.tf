# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-sqs
module "queue" {
  source            = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-sqs.git?ref=v2"
  name              = "dapi-sqs-poc-queue-one"
  namespace         = module.namespace.lower_short_name
  tags              = module.namespace.tags
  fifo_queue        = true
  consumer_arn      = [module.lambda.role.arn]
  max_receive_count = 4
}

# Documentation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping
resource "aws_lambda_event_source_mapping" "queue_event_source" {
  event_source_arn = module.queue.queue.arn
  function_name    = module.lambda.arn
}