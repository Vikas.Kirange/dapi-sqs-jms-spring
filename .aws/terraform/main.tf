data "aws_caller_identity" "current" {}

module "namespace" {
  source              = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-name-and-tags.git?ref=v1"
  application_name    = var.application_name
  service_name        = var.service_name
  workspace           = terraform.workspace
  environment         = var.environment
  application_id      = var.application_id
  data_classification = var.data_classification
  issrcl_level        = var.issrcl_level
  owner               = var.owner
  scm_project         = var.scm_project
  scm_repo            = var.scm_repo
  scm_branch          = var.scm_branch
  scm_commit_id       = var.scm_commit_id
  additional_tags = {
    "tf_starter"       = var.creation_date
    "cloud_pattern_id" = "CSP-9"
  }
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-state/browse
module "state" {
  source        = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-state.git?ref=v2"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
}

module "lambda" {
  source        = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v6"
  namespace     = module.namespace.lower_short_name
  name          = "fm-msg-pub"
  environment   = var.environment
  code          = "../../src/"
  handler       = "main.lambda_handler"
  timeout       = 30
  size          = 128
  runtime       = "python3.8"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
  env_vars = {
    S3_BUCKET_NAME = module.bucket.id
    QUEUE_URL      = module.queue.queue.id
  }
  execution_policy = data.aws_iam_policy_document.lambda_policy.json
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-sqs/browse
module "sqs" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-sqs.git?ref=v2"
  namespace   = module.namespace.lower_resource_name
  name        = "dapi-sqs-poc-queue-one"
  region      = var.region
  environment = var.environment

  max_receive_count = 10
  tags              = module.namespace.tags
}
data "aws_iam_policy_document" "lambda_policy" {
  statement {
    sid    = "AllowS3"
    effect = "Allow"
    actions = [
      "s3:*"
    ]
    resources = [
      module.bucket.arn,
      "${module.bucket.arn}/*"
    ]
  }

  statement {
    sid    = "AllowKMS"
    effect = "Allow"
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = [
      "arn:aws:kms:${var.region}:${data.aws_caller_identity.current.account_id}:alias/aws/s3"
    ]
  }

}

