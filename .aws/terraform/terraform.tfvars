# Name & Tagging
application_id      = "104964"
application_name    = "nzzptg"
service_name        = "dapi-sqs-poc"
owner               = "nzzptg"
data_classification = "Proprietary"
issrcl_level        = "Low"
scm_project         = "allyfinancial/consumer-commercial-banking-technology/deposits-api/services"
scm_repo            = "dapi-sqs-poc"

awsacct_cidr_code = {
  default = "010-073-112-000"
  dev     = "010-073-112-000"
  qa      = "010-073-128-000"
  cap     = "010-073-128-000"
  psp     = "010-073-160-000"
  prod    = "010-073-160-000"
}

